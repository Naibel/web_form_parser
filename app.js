const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const fs = require('fs-extra')
const debug = require('debug')('formApp')
const _ = require('lodash')
const async = require('async')
const chalk = require('chalk')
const parse = require('csv-parse/lib/sync')
const stringify = require('csv-stringify')
const app = express();

let red = chalk.red
let green = chalk.green
let file
let data
let entries
let language

function splitUp (arr, n) {
  var rest = arr.length % n // how much to divide
  var restUsed = rest // to keep track of the division over the elements
  var partLength = Math.floor(arr.length / n)
  var result = []

  for (var i = 0; i < arr.length; i += partLength) {
    var end = partLength + i
    var add = false

    if (rest !== 0 && restUsed) { // should add one element for the division
      end++
      restUsed-- // we've used one division element now
      add = true
    }

    result.push(arr.slice(i, end)) // part of the array

    if (add) {
      i++ // also increment i in the case we added an extra element for division
    }
  }

  return result
}

/**
 * extracts
 * - the franchisee entry (20 first array items)
 * - the other entries in an array of arrays
 * @param  {array} entry
 */
function getEntries (entry, index, headerCol) {
  var firstEntry;
  var allCollabs;

  if(language=='it'){
  	firstEntry = entry.slice(0, 9)
  	allCollabs = entry.slice(9)
  }
  else if(language=='fr'){
  	firstEntry = entry.slice(0, 11)
  	allCollabs = entry.slice(11)
  }

  var errorCollabs = []

  var collabsArr = splitUp(allCollabs, 20)
    .filter(function (colab, index) {
      if (colab[0] === '') {
        return false
      }

      // index 2 => code salon
      let codeSalon = colab[2]
      let firstLetter = codeSalon.split('')[0]

      	if(language=='fr'){
	      if (firstLetter !== 'B' && firstLetter !== 'N' && firstLetter !== 'C' &&
	          firstLetter !== 'F' && firstLetter !== 'G' && firstLetter !== 'H' &&
	          firstLetter !== 'M' && firstLetter !== 'P' && firstLetter !== 'S' &&
	          firstLetter !== 'W' && firstLetter !== 'I' && firstLetter !== 'D' &&
	          firstLetter !== 'K' && firstLetter !== '') {
	        errorCollabs.push(colab)
	        return false
	      }
  		}

      colab.pop()
      return true
    })

  if (headerCol) return { franchisee: firstEntry, collabs: collabsArr[0], collabsError: collabsArr[0] }

  return { franchisee: firstEntry, collabs: collabsArr, collabsError: errorCollabs }
}

function getHeaders (entries) {
  var headRow = entries.shift()
  var headArr = getEntries(headRow, 0, true)
  return headArr
}

function treatForms (entries) {
  var iter = 0
  var allForms = []
  async.each(entries, function (entry, callback) {
    debug('Processing entry ' + iter + 1)
    var entries = getEntries(entry, iter)
    console.log(entries)
    debug(entries)
    allForms.push(entries)
    debug('entry processed')
    ++iter
    callback()
  }, function (err) {
    if (err) {
      debug('An entry failed to process')
    } else {
      debug('All entries have been processed successfully')

      var allCsvArr = makeCsvArrays(headers)

      async.each(allForms, function (form, cb) {
        treatEntry(form, allCsvArr)
        cb()
      }, function (err, results) {
        if (err) console.error(err)
        stringify(allCsvArr.franchisees, function (err, output) {
          if (err) console.log(err)
          fs.writeFileSync('./output/franchesee.csv', output)
          console.log(green('file written', 'franchesee.csv'))
        })
        stringify(allCsvArr.collaborators, function (err, output) {
          if (err) console.log(err)
          fs.writeFileSync('./output/collabs.csv', output)
          console.log(green('file written', 'collabs.csv'))
        })
        stringify(allCsvArr.collaboratorsError, function (err, output) {
          if (err) console.log(err)
          fs.writeFileSync('./output/collabsError.csv', output)
          console.log(green('file written', 'collabsError.csv'))
        })

      })
      return allForms
    }
  })
}

function treatEntry (entry, allArrObj) {
  makeFranchesee(entry.franchisee, allArrObj.franchisees)
  makeCollabs(entry.collabs, allArrObj.collaborators)
  makeCollabs(entry.collabsError, allArrObj.collaboratorsError)
  // makeCollaboratorsError(entry.collabsError, allArrObj.collaboratorsError)

  return allArrObj
}

function makeCsvArrays (headers) {
  var container = {}

  container.franchisees = []
  container.collaborators = []
  container.collaboratorsError = []

  container.franchisees.push(headers.franchisee)
  container.collaborators.push(headers.collabs)
  container.collaboratorsError.push(headers.collabsError)

  return container
}

function makeFranchesee (entries, array) {
  entries[1] = entries[1].toUpperCase()
  entries[2] = entries[2].toUpperCase()
  array.push(entries)

  return array
}

function makeCollabs (entries, array) {
  _.each(entries, function (entry, key) {
    entry[0] = entry[0].toUpperCase()
    entry[1] = entry[1].toUpperCase()
    entry[3] = entry[3].toLowerCase()
    if (entry[5] === '') entry[5] = 'pas de téléphone'
    // else if (/\+41/i.test(entry[6])) console.log(entry[0] + ' ' + entry[1] + ' ' + entry[5] + ' ' + entry[6])
    else entry[5] = '' + entry[5]
    array.push(entry)
  })
  return array
}

//utiliser le module de parcing
app.use(bodyParser.urlencoded({extended: true})).use(express.static(__dirname + '/assets')).use(fileUpload());
app.listen(8080);

app.get('/', function(request, response) {
  // var p1 = request.params.p1;
  response.sendFile(__dirname + '/page.html')
});

app.get('/download/:filename', function(req, res){
  var filename = __dirname + '/output/' + req.params.filename;
  res.download(filename);
});

app.post('/upload', function(req, res){
  if(_.isEmpty(req.files))
    return res.status(400).render('error.ejs', {errorMessage: 'Vous n\'avez uploadé aucun fichier.'});

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let fileUpload = req.files.fileUpload;
  // res.render('chambre.ejs', {etage: req.params.etagenum});

  // Use the mv() method to place the file somewhere on your server
  fileUpload.mv('./src/' + req.files.fileUpload.name, function(err) {
    if (err)
      return res.status(500).render('error.ejs', {errorMessage: 'Error d\'upload de fichier.'});


    file = './src/' + req.files.fileUpload.name;
    data = fs.readFileSync(file, 'utf8')
    entries = parse(data)
    language = req.body.lang;

    headers = getHeaders(entries)
    treatForms(entries)

    res.render('upload.ejs', {urlCollabs: '/download/collabs.csv', urlCollabsError: '/download/collabsError.csv', urlFranchesee: '/download/franchesee.csv'});
    // res.sendFile(__dirname + '/post.html')
  });

});
