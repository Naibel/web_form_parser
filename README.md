# form extractor Web
> il extrait plus vite que son ombre - sur le web !

## prerequis
nodejs > 0.12.x

## utilisation

- dans google doc exporter le ficher au format csv
- Renommez ce fichier en la date d'aujourd'hui (jj-mm-aa)
- Sur le formulaire de la page d'accueil, uploadez le fichier csv
- Appuyez sur le bouton 'Traiter le fichier'.
- Si tout se passe bien, sur la page suivante, téléchargez les trois fichiers traités.
- importer les fichiers ```collabs.csv```,  ```collabsError.csv``` et  ```franchesee.csv``` dans google doc
